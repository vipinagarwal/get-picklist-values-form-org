import { LightningElement, track } from 'lwc';
import fetchPicklist from "@salesforce/apex/GetBasicData.fetchPicklist";
import { NavigationMixin } from 'lightning/navigation';
export default class BanNeighbourNeedModelWindow extends NavigationMixin(LightningElement) {
    connectedCallback() {
            fetchPicklist({
                objectName: 'Account',
                fieldName: 'Industry'
            })
            .then(result => {
                this.Industry = result;
            })
            .catch(error => {
                let message = error.message || error.body.message;
                console.log('Error: ' + message);
            });
    }
}